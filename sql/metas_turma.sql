-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 22-Jun-2019 às 20:46
-- Versão do servidor: 10.1.38-MariaDB
-- versão do PHP: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lp2_modulo`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `metas_turma`
--

CREATE TABLE `metas_turma` (
  `id` int(11) NOT NULL,
  `ano` varchar(6) NOT NULL,
  `nivel` varchar(50) NOT NULL,
  `turma` varchar(1) NOT NULL,
  `periodo` varchar(20) NOT NULL,
  `meta` varchar(255) NOT NULL,
  `prazo` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `metas_turma`
--

INSERT INTO `metas_turma` (`id`, `ano`, `nivel`, `turma`, `periodo`, `meta`, `prazo`) VALUES
(2, '6º Ano', 'Ensino Fundamental', 'B', 'Tarde', 'Aumentar frequência da turma em 20%', '05/09/2019'),
(3, '1º Ano', 'Ensino Médio', 'C', 'Noite', 'Melhorar média de notas da turma em matemática 25%.', '12/12/2019');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `metas_turma`
--
ALTER TABLE `metas_turma`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `metas_turma`
--
ALTER TABLE `metas_turma`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
