<?php 
defined('BASEPATH') OR exit('No direct script access allowed');


class Validator extends CI_Object{

    public function form_tarefa(){
        $this->form_validation->set_rules('ano', 'ano', 'trim|required');
        $this->form_validation->set_rules('nivel', 'nivel', 'trim|required');
        $this->form_validation->set_rules('turma', 'turma', 'trim|required');
        $this->form_validation->set_rules('periodo', 'periodo', 'trim|required');
        $this->form_validation->set_rules('prazo', 'prazo', 'trim|required|exact_length[10]');
        return $this->form_validation->run();
    }

}