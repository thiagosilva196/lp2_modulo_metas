<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class MetasTurma extends Dao {

    function __construct(){
        parent::__construct('metas_turma');
    }

    public function insert($data, $table = null) {
        // mais uma camada de segurança... além da validação
        $cols = array('ano', 'nivel', 'turma', 'periodo', 'prazo', 'meta');
        $this->expected_cols($cols);

        return parent::insert($data);
    }

    public function get($conds = null, $table = null){
        $cols = array('*');
        $this->cols($cols);

        return parent::get();
    }

    
}