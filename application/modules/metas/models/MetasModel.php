<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

class MetasModel extends CI_Model{

    public function nova_meta($data){

        if(sizeof($data) == 0) return;
        $this->load->library('Validator', null, 'valida');

        if($this->valida->form_tarefa()){
            $this->load->library('MetasTurma', null, 'metas');
            //$data = $this->input->post();
            $this->metas->insert($data);
            return true;
        }
        else return false;

    }

    public function lista_metas(){
        $this->load->library('MetasTurma', null, 'metas');
        $data = $this->metas->get();
        return $data;
    }
}
?>