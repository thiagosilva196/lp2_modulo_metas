<!-- Default form contact -->
<div class="container col-4">

    <form class="text-center border border-light p-5" action= "<?php echo $action ?>" method="POST">
    <?php echo validation_errors(); ?>
        <p class="h4 mb-4">Nova Meta</p>

        <p>Ano: </p><input type="text" name ="ano" id="ano" class="form-control mb-4" placeholder="Ex: 1º Ano">

        <label>Nível</label>
        <select class="browser-default custom-select mb-4" name ="nivel" id="nivel">
            <option selected disabled>Selecione...</option>
            <option value="Ensino Fundamental">Ensino Fundamental</option>
            <option value="Ensino Médio">Ensino Médio</option>
        </select>

        <label>Turma</label>
        <select class="browser-default custom-select mb-4" name ="turma" id="turma">
            <option value="" selected disabled>Selecione...</option>
            <option value="A" >A</option>
            <option value="B">B</option>
            <option value="C">C</option>
            <option value="D">D</option>
            <option value="E">E</option>
            <option value="F">F</option>
        </select>

        <label>Período</label>
        <select class="browser-default custom-select mb-4" name="periodo" id="periodo">
            <option value="" selected disabled>Selecione...</option>
            <option value="Manhã">Manhã</option>
            <option value="Tarde">Tarde</option>
            <option value="Noite">Noite</option>
        </select>

        <p>Prazo: </p><input type="text" name = "prazo" id="prazo" class="form-control mb-4" placeholder="dd/mm/aaaa">

        <!-- Message -->
        <div class="form-group">
            <textarea class="form-control rounded-0" name = "meta" id="meta" rows="3" placeholder="Descrição da meta"></textarea>
        </div>

        <!-- Send button -->
        <button class="btn btn-info btn-block" type="submit">Salvar</button>
        </a>
        <a href="<?php echo base_url();?>metas">
            <button class="btn btn-info btn-block" type="button">Cancelar</button>
        </a>

    </form>
    <!-- Default form contact -->
</div>