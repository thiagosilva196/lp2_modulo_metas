<div class="container col-10">
<!--Table-->
<table class="table table-striped">

  <!--Table head-->
  <thead>
    <tr>
      <th>#</th>
      <th>Ano</th>
      <th>Nível</th>
      <th>Turma</th>
      <th>Período</th>
      <th>Meta</th>
      <th>Prazo</th>
    </tr>
  </thead>
  <!--Table head-->

  <!--Table body-->
  <tbody>
    <tr class="table-info">
      <?php 
        foreach ($tabela as $row){
          echo "<th scope='row'>".$row['id']."</th>
          <td>".$row['ano']."</td>
          <td>".$row['nivel']."</td>
          <td>".$row['turma']."</td>
          <td>".$row['periodo']."</td>
          <td>".$row['meta']."</td>
          <td>".$row['prazo']."</td>
          </tr>";
        }
      ?>   
  </tbody>
  <!--Table body-->
</table>

<a href="<?php echo base_url();?>metas/nova_meta">
    <button type="button" class="btn btn-primary">Nova Meta</button>
</a>
      </div>
<!--Table-->