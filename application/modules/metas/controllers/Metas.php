<?php 
defined('BASEPATH') OR exit('No direct script access allowed');


class Metas extends MY_Controller{

    
    public function index(){

        $this->load->view('common/cabecalho');
        $this->load->model('MetasModel', 'metas');
        $data['tabela'] = $this->metas->lista_metas();
        $this->load->view('metas', $data);
        $this->load->view('common/rodape');

    }

    public function nova_meta(){
        $this->load->view('common/cabecalho');
        $data['action'] = base_url('metas/salva_meta');
        $this->load->view('meta_form', $data);
        $this->load->view('common/rodape');
    }

    public function salva_meta(){
        $this->load->view('common/cabecalho');
        $post = $this->input->post();
        $this->load->model('MetasModel', 'metas');
        $save = $this->metas->nova_meta($post);
        if($save == false){
            $this->load->view('msg_erro');
        }
        elseif ($save == true){
            $this->load->view('msg_sucesso');
        }
        $this->load->view('common/rodape');
        $post = null;

    }

    

}