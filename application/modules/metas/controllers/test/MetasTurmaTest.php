<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH . '/controllers/test/Toast.php';
include_once APPPATH . 'modules/metas/libraries/MetasTurma.php';
include_once APPPATH . 'modules/metas/controllers/test/builder/MetasTurmaDataBuilder.php';

class MetasTurmaTest extends Toast{
    private $builder;
    private $metas;

    function __construct(){
        parent::__construct('MetasTurmaTest');
    }

    function _pre(){
        $this->builder = new MetasTurmaDataBuilder();
        $this->metas = new MetasTurma();
    }

    // apenas ilustrativo
    function test_objetos_criados_corretamente(){
        $this->_assert_true($this->builder, "Erro na criação do builder");
        $this->_assert_true($this->metas, "Erro na criação da meta");
    }

    // apenas ilustrativo
    function test_selecionado_banco_de_teste(){
        $s = $this->builder->database();
        $this->_assert_equals('lp2_modulo', $s, 'Erro na seleção do banco de teste');
    }

    function test_insere_registro_na_tabela(){
        // cenário 1: vetor com dados corretos
        $this->builder->clean_table();
        $data = $this->builder->getData(0);
        $id1 = $this->metas->insert($data);
        $this->_assert_equals(1, $id1, "Esperado 1, recebido $id1");

        // verificação: o objeto criado é, de fato, aquele que enviamos?
        $task = $this->metas->get(array('id' => 1))[0];
        $this->_assert_equals($data['ano'], $task['ano']);
        $this->_assert_equals($data['turma'], $task['turma']);
        $this->_assert_equals($data['periodo'], $task['periodo']);
        $this->_assert_equals($data['meta'], $task['meta']);

        // cenário 2: vetor vazio
        $id2 = $this->metas->insert(array());
        $this->_assert_equals(-1, $id2, "Esperado -1, recebido $id2");

        // cenário 3: vetor com dados inesperados
        $info = $this->builder->getData(1);
        $info['unexpected_col_name'] = 1;
        $id = $this->metas->insert($info);
        $this->_assert_equals(-1, $id, "Esperado -1, recebido $id");

        // cenário 4: vetor com dados incompletos
        $v = array('titulo' => 'vetor incompleto');
        $id = $this->metas->insert($v);
        $this->_assert_equals(-1, $id, "Esperado -1, recebido $id");

        }

        function test_carrega_todos_os_registros_da_tabela(){
            $this->builder->clean_table();
            $this->builder->build();
    
            $tasks = $this->metas->get();
            $this->_assert_equals(3, sizeof($tasks), "Número de registros incorreto");
    }

        function test_carrega_registro_condicionalmente(){
            $this->builder->clean_table();
            $this->builder->build();

            $task = $this->metas->get(array('prazo' => '12/10/2019', 'turma' => 'A'))[0];
            $this->_assert_equals('12/10/2019', $task['prazo'], "Erro no prazo");
            $this->_assert_equals('A', $task['turma'], "Erro na turma da meta");
        }

        function test_atualiza_registro(){
            $this->builder->clean_table();
            $this->builder->build();
    
            // lê um registro do bd
            $task1 = $this->metas->get(array('id' => 1))[0];
            $this->_assert_equals('12/10/2019', $task1['prazo'], "Erro no prazo");
            $this->_assert_equals('A', $task1['turma'], "Erro na turma");
    
            // atualiza seus valores
            $task1['prazo'] = '21/06/2019';
            $task1['turma'] = 'E';
            $this->metas->insert_or_update($task1);
    
            // lê novamente, o mesmo objeto, e verifica se foi atualizado
            $task2 = $this->metas->get(array('id' => 1))[0];
            $this->_assert_equals($task1['prazo'], $task2['prazo'], "Erro no prazo");
            $this->_assert_equals($task1['turma'], $task2['turma'], "Erro na turma");
        }

        function test_remove_registro_da_tabela(){
            $this->builder->clean_table();
            $this->builder->build();
    
            // verifica que o registro existe
            $task1 = $this->metas->get(array('id' => 1))[0];
            $this->_assert_equals('12/10/2019', $task1['prazo'], "Erro no prazo");
            $this->_assert_equals('A', $task1['turma'], "Erro na turma");
    
            // remove o registro
            $this->metas->delete(array('id' => 1));
    
            // verifica que o registro não existe mais
            $task2 = $this->metas->get(array('id' => 1));
            $this->_assert_equals_strict(2, sizeof($task2), "Registro não foi removido");
        }

        
}