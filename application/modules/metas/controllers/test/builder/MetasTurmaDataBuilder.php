<?php
include_once APPPATH.'controllers/test/builder/TestDataBuilder.php';

class MetasTurmaDataBuilder extends TestDataBuilder {

    public function __construct($table = 'lp2_modulo'){
        parent::__construct('metas_turma', $table);
    }

    function getData($index = -1){
        $data[0]['id'] = '1';
        $data[0]['ano'] = '5º Ano';
        $data[0]['nivel'] = 'Ensino Fundamental';
        $data[0]['turma'] = 'A';
        $data[0]['periodo'] = 'Manhã';
        $data[0]['meta'] = 'Melhorar média de notas da turma em 50%.';
        $data[0]['prazo'] = '12/10/2019';

        $data[1]['id'] = '2';
        $data[1]['ano'] = '6º Ano';
        $data[1]['nivel'] = 'Ensino Fundamental';
        $data[1]['turma'] = 'B';
        $data[1]['periodo'] = 'Tarde';
        $data[1]['meta'] = 'Aumentar frequência da turma em 20%';
        $data[1]['prazo'] = '05/09/2019';

        $data[2]['id'] = '3';
        $data[2]['ano'] = '1º Ano';
        $data[2]['nivel'] = 'Ensino Médio';
        $data[2]['turma'] = 'C';
        $data[2]['periodo'] = 'Noite';
        $data[2]['meta'] = 'Melhorar média de notas da turma em matemática 25%.';
        $data[2]['prazo'] = '12/12/2019';


        return $index > -1 ? $data[$index] : $data;
    }

}